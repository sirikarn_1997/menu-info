import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import Axios from "axios";
import "./index.css";
import PerfectScrollbar from "vue2-perfect-scrollbar";
import VueSweetalert2 from 'vue-sweetalert2';
import "vue2-perfect-scrollbar/dist/vue2-perfect-scrollbar.css";
import titleMixin from './titleMixin'


Vue.config.productionTip = false;


Vue.mixin(titleMixin)
Vue.use(VueSweetalert2);
import "sweetalert2/dist/sweetalert2.min.css";
Vue.use(PerfectScrollbar);
new Vue({
    router,
    store,
    Axios,
    render: (h) => h(App),
}).$mount("#app");