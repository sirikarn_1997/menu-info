import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [{
        path: "/",
        name: "",
        //component: Home,
        component: () =>
            import ( /* webpackChunkName: "about" */ "../views/Mainpage"),

    }, {
        path: "/crep",
        name: "",
        //component: Home,
        component: () =>
            import ( /* webpackChunkName: "about" */ "../views/CreatProfile"),

    }, {
        path: "/pro_all",
        name: "",
        //component: Home,
        component: () =>
            import ( /* webpackChunkName: "about" */ "../views/ProfileAllpage"),

    }, {
        path: "pro_edit/:name",
        name: "edit",
        //component: Home,
        component: () =>
            import ( /* webpackChunkName: "about" */ "../views/EditProfile"),

    },

];

const router = new VueRouter({
    mode: "history",
    base: process.env.BASE_URL,
    routes,
});

export default router;